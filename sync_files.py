import sys
import threading
from datetime import datetime

import boto3
from boto3.s3.transfer import TransferConfig
import os

s3_client = boto3.client("s3")


class ProgressPercentage(object):
    def __init__(self, filename):
        self._filename = filename
        self._size = float(os.path.getsize(filename))
        self._seen_so_far = 0
        self._lock = threading.Lock()

    def __call__(self, bytes_amount):
        with self._lock:
            self._seen_so_far += bytes_amount
            percentage = (self._seen_so_far / self._size) * 100
            sys.stdout.write(
                "\r%s  %s / %s  (%.2f%%)"
                % (self._filename, self._seen_so_far, self._size, percentage)
            )
            sys.stdout.flush()


def upload_files(directory: str, bucket: str):
    config = TransferConfig(
        multipart_threshold=1024 * 25,
        max_concurrency=10,
        multipart_chunksize=1024 * 25,
        use_threads=True,
    )

    for filename in os.listdir(directory):
        file_path = os.path.join(directory, filename)
        if os.path.isfile(file_path):
            # Get the last modified time and format it
            last_modified_time = os.path.getmtime(file_path)
            date = datetime.fromtimestamp(last_modified_time)
            year_month = date.strftime("%Y/%B")  # e.g. '2023/April'

            key = f"{year_month}/{filename}"

            print(f"Starting upload for {file_path} to {key}")
            s3_client.upload_file(file_path, bucket, key,
                                  ExtraArgs={'ContentType': 'text/plain'},
                                  Config=config,
                                  Callback=ProgressPercentage(file_path))
            print(f"Upload finished for {file_path}")


def main():
    from argparse import ArgumentParser

    parser = ArgumentParser(description="Upload files to S3.")
    parser.add_argument(
        "directory", help="Path to the directory containing files to upload"
    )
    parser.add_argument("s3_bucket", help="Name of the S3 bucket")

    args = parser.parse_args()

    upload_files(args.directory, args.s3_bucket)


if __name__ == "__main__":
    main()
